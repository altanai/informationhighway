/*
 * BlueToothExp.java
 *
 * Created on December 8, 2006, 2:08 AM
 */

import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

/**
 * @author Farhan Hameed Khan
 */
public class BlueToothExp extends MIDlet implements CommandListener, Runnable {

    /**
     * Creates a new instance of BlueToothExp
     */
    public BlueToothExp() {
        initialize();
    }

    private List list1;//GEN-LINE:MVDFields
    private ClientServer m_BlueObj;
    private boolean m_bRunThread = false;
    private boolean m_bIsServer = false;


//GEN-LINE:MVDMethods

    /**
     * Called by the system to indicate that a command has been invoked on a particular displayable.//GEN-BEGIN:MVDCABegin
     *
     * @param command     the Command that ws invoked
     * @param displayable the Displayable on which the command was invoked
     */
    public void commandAction(Command command, Displayable displayable) {//GEN-END:MVDCABegin
        // Insert global pre-action code here
        if (displayable == list1) {//GEN-BEGIN:MVDCABody
            if (command == list1.SELECT_COMMAND) {
                switch (get_list1().getSelectedIndex()) {
                    case 0://GEN-END:MVDCABody
                        // Insert pre-action code here
                        // Do nothing//GEN-LINE:MVDCAAction5
                        // Insert post-action code here

                        break;//GEN-BEGIN:MVDCACase5
                    case 1://GEN-END:MVDCACase5
                        // Insert pre-action code here
                        // Do nothing//GEN-LINE:MVDCAAction7
                        // Insert post-action code here
                        //SERVER
                        if (m_bRunThread == false) {
                            Thread thread = new Thread(this);
                            thread.start();
                            m_bRunThread = true;
                            m_bIsServer = true;
                        }


                        break;//GEN-BEGIN:MVDCACase7
                    case 2://GEN-END:MVDCACase7
                        // Insert pre-action code here
                        //CLIENT
                        if (m_bRunThread == false) {
                            Thread thread = new Thread(this);
                            thread.start();
                            m_bRunThread = true;
                            m_bIsServer = false;
                        }
                        // Do nothing//GEN-LINE:MVDCAAction9
                        // Insert post-action code here
                        break;//GEN-BEGIN:MVDCACase9
                }
            }
        }//GEN-END:MVDCACase9
        // Insert global post-action code here
    }//GEN-LINE:MVDCAEnd

    /**
     * This method initializes UI of the application.//GEN-BEGIN:MVDInitBegin
     */
    private void initialize() {//GEN-END:MVDInitBegin
        // Insert pre-init code here
        getDisplay().setCurrent(get_list1());//GEN-LINE:MVDInitInit
        // Insert post-init code here
    }//GEN-LINE:MVDInitEnd

    /**
     * This method should return an instance of the display.
     */
    public Display getDisplay() {//GEN-FIRST:MVDGetDisplay
        return Display.getDisplay(this);
    }//GEN-LAST:MVDGetDisplay

    /**
     * This method should exit the midlet.
     */
    public void exitMIDlet() {//GEN-FIRST:MVDExitMidlet
        getDisplay().setCurrent(null);
        destroyApp(true);
        notifyDestroyed();
    }//GEN-LAST:MVDExitMidlet

    /**
     * This method returns instance for list1 component and should be called instead of accessing list1 field directly.//GEN-BEGIN:MVDGetBegin2
     *
     * @return Instance for list1 component
     */
    public List get_list1() {
        if (list1 == null) {//GEN-END:MVDGetBegin2
            // Insert pre-init code here
            list1 = new List(null, Choice.IMPLICIT, new String[]{//GEN-BEGIN:MVDGetInit2
                    "Client Server",
                    "Server",
                    "Client"
            }, new Image[]{
                    null,
                    null,
                    null
            });
            list1.setCommandListener(this);
            list1.setSelectedFlags(new boolean[]{
                    false,
                    false,
                    false
            });//GEN-END:MVDGetInit2
            // Insert post-init code here
        }//GEN-BEGIN:MVDGetEnd2
        return list1;
    }//GEN-END:MVDGetEnd2

    public void startApp() {
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {

        m_bRunThread = false;
        m_BlueObj.CloseAll();
    }

    public void run() {
        while (m_bRunThread) {
            try {
                if (m_BlueObj == null) {
                    m_BlueObj = new ClientServer(m_bIsServer);
                }

                String str = m_BlueObj.RecieveMessages();

                System.out.println(str);

                if (m_bIsServer)
                    m_BlueObj.SendMessages("Hi there its Mr Server");
                else
                    m_BlueObj.SendMessages("Hi there its Mr Client");

                Thread.sleep(100);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

        }
    }//end while

}