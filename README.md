# README #

To meet the requirements of an intelligent vehicle authentication, management and monitoring system, the software integrates GPS( Global positioning system) ,GSM( Global system for Mobile Communication) and GIS(Global Information System) . 
The authentication is carried out in between the vehicle and driver by means of digital session keys ( major key and minor key ) which are generated in server side application and supplied to vehicle and driver separately . Access is granted if the driver enters the right key and not granted access when the key entered is wrong. Also on expiry of the session the driver needs to request for fresh sets of keys from the server again.
There is a custom random key generator process, to create unpredictable keys for every occasion.  The real time vehicle monitoring is done by interconnection between the GPS enabled mobile phone and server side application via email service to transport the coordinates. Server displays them onto a map to trace the location of the driver, hence the vehicle. 

* [Information Highway Archieved Project](https://www.researchgate.net/project/Information-Highway-Digital-key-controlled-Vehicle-Access-system-2011)

### Architecture ###

![img](diagrams/informationhighway8.jpg)

![img](diagrams/informationhighway7.jpg)

### Screenshots ###

![img](screenshots/informationhighway1.jpg)

![img](screenshots/informationhighway2.jpg)

![img](screenshots/informationhighway3.jpg)

![img](screenshots/informationhighway4.jpg)

![img](screenshots/informationhighway5.jpg)

![img](screenshots/informationhighway6.jpg)

### License ###

* MIT
( it's an old project but feel free to use it however best you can)