

import javax.mail.*;
import javax.mail.internet.*;
import java.security.Security;
import java.util.*;

public class send_mail_keys_driver {
    int WID, sk1, sk2;
    String from = "xyz@gmail.com";
    String d_password = "xxx";
    String d_host = "smtp.gmail.com";
    String d_port = "587";
    String receipent = "abc@gmail.com";
    String m_subject = "KEY";
    String m_text = "not permitted";

    public send_mail_keys_driver(int WID, int sk1, int sk2, String receipent) {
        WID = this.WID;
        sk1 = this.sk1;
        sk2 = this.sk2;
        receipent = this.receipent;
        m_text = f(sk1) + String.valueOf(sk2);
        System.setProperty("javax.net.debug", "ssl");
        Properties props = new Properties();
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.host", d_host);
        props.put("mail.smtp.port", d_port);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.debug", "true");
        props.put("mail.smtp.socketFactory.port", d_port);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        try {
            SMTPAuthenticator auth = new athenticator();
            Session session = Session.getInstance(props);
            session.setDebug(true);
            MimeMessage msg = new MimeMessage(session);
            msg.setText(m_text);
            msg.setSubject(m_subject);
            msg.setFrom(new InternetAddress(from));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(receipent));
            Transport.send(msg);
        } catch (Throwable mex) {
            mex.printStackTrace();
        }
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(from, d_password);
        }
    }
}
