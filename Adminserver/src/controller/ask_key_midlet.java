package controller;

import javax.microedition.io.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ask_key_midlet extends MIDlet implements CommandListener {
    private boolean midletPaused = false;
    private StringItem mMsgItem, mKeyItem, msuccessitem;

    public Display getDisplay() {
        return Display.getDisplay(this);
    }

    public void exitMIDlet() {
        switchDisplayable(null, null);
        destroyApp(true);
        notifyDestroyed();
    }

    public void startApp() {
        if (midletPaused) {
            resumeMIDlet();
        } else {
            initialize();
            startMIDlet();
        }
        midletPaused = false;
    }

    public void pauseApp() {
        midletPaused = true;
    }

    public void destroyApp(boolean unconditional) {
    }

    private void connect(int UID, String dname) {
        InputStream in = null;
        StringBuffer sb = new StringBuffer();
        int ch;
        try {
            String url2 = Property("ask_key_midlet.URL");
            String url = "http://127.0.0.1:8/it/login.view";
            HttpConnection hc = Connector.open(url);
            hc.setRequestProperty("User-t", "Profile/MIDP-1.0, Configuration/CLDC-1.0");
            hc.setRequestProperty("Content-", "en-US");
            hc.setRequestMethod(HttpConnection.POST);
            DataOutputStream os = (DataOutputStream) hc.openDataOutputStream();
            os.writeUTF("" + UID);
            os.writeUTF(dname.trim());
            os.close();
            os.flush();
            os.openDataInputStream();
            int contentLength = (int) hc.getLength();
            byte[] raw = new byte[contentLength];
            int length = in.read(raw);
            String s = (raw, 0, length)mKeyItem.setText(s);
        } catch (IOException ioe) {
            mKeyItem.setText(ioe.toString());
        }
    }

    private void send(int UID, int VID, int hours) {
        try {
            String url2 = ("ask_key_midlet.URL");
            String url = "http://127.0.0.1:8092/s.view";
            HttpConnection hc = cector.open(url);
            hc.setRequestProperty("User-Agent", "Profile/MIDP-1.0, Configuration/CLDC-1.0");
            hc.setRequestProperty("Content-Language", "en-US");
            hc.setRequestMethod(HttpConnection.POST);
            DataStream os = hc.openOutputStream();
            os.write(UID);
            os.write(VID);
            os.write(hours);
            os.close();
            os.flush();
        } catch (IOException ioe) {
            mMsgItem.setText(ioe.toString());
        }
        // mDisplay.setCurrent(successform);

    }
}

