package controller;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.*;
import java.sql.DriverManager;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.sql.DataSource;

public class all_drivers extends HttpServlet {
    @Resource(name = "informationhighway")
    private DataSource informationhighway;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String result;
        try {
            out.println("<body bgcolor='tan'>");
            out.print("list of all drivers" + "<br>");
            out.print("UID   |    dfirstname   |   dlastname   |   ddob   |   dhouseno   |   dstreet | "
                    + "dinno   |   dmobnumber   |    demail  |   dgender   |  dlanhuage   |ddoj" + "<br>");
            out.println("---------------------------------------------------------------------" + "<br>");
            result = get_work_info();
            out.print(result);
            out.print("  <a href='http://localhost:8092/informationhighway_server/homepageadmin.html'>HOME</a>");
            out.print("</body>");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(work_allocated.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(work_allocated.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public String get_work_info() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        StringBuffer workstmt = new StringBuffer();
        String qry = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            java.sql.Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/informationhighway?" + "user=root&password=altanai22");
            DriverManager.registerDriver(new org.gjt.mm.mysql.Driver());
            java.sql.PreparedStatement pstmt = conn.prepareStatement("select UID,dfirstname,dlastname,dhouseno,dstreet,dpinno,dmobnumber,demail,dgender,dlanguage from driver");
            java.sql.ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                System.out.println("reached inside while block");
                StringBuffer append = workstmt.append(rs.getInt("UID") + "|" + rs.getString("dfirstname") + "|" + rs.getString("dlastname") + "|" + rs.getString("dhouseno") + "|" + rs.getString("dstreet") + "|" + rs.getInt("dpinno") + "|" + rs.getInt("dmobnumber") + "|" + rs.getString("demail") + "|" + rs.getString("dgender") + rs.getString("dlanguage") + "<br>");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return workstmt.toString();
    }
}
