import bluetooth.BluetoothStateException;

import javax.bluetooth.*;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ClientServer implements Listener {
    /**
     * Creates a new instance of ClientServer
     */
    UUID RFCOMM_UUID = new UUID(0x0003);
    private final String m_ServerUrl = "btspp://localhost:" + RFCOMM_UUID + ";name=rfcommtest;authorize=true";
    private StreamConnection m_StrmConn = null;
    private LocalDevice m_LclDevice = null;
    private InputStream m_Input = null;
    private OutputStream m_Output = null;
    private StreamNotifier m_StrmNotf = null;
    public boolean m_bIsServer = false, m_bServerFound = false, m_bInitServer = false, m_bInitClient = false;
    private static String m_strUrl;
    private final String SEVER_RESPONSE = "run", CLIENT_RESPONSE = "ready";
    private DiscoveryAgent m_DscrAgent = null;

    public ClientServer(boolean isServer) {
        m_bIsServer = isServer;
        if (m_bIsServer) {
            InitServer();
        } else {
            InitClient();
        }
    }

    private void InitServer() {
        m_strUrl = "btspp://localhost:" + RFCOMM_UUID + ";name=rfcommtest;authorize=true";
        try {
            m_LclDevice = LocalDevice.getLocalDevice();
            m_LclDevice.setDiscoverable(Discover.GIAC);
            m_StrmNotf = Connector.open(m_strUrl);
            m_StrmConn = m_StrmNotf.acceptAndOpen();
            m_bInitServer = true;
            m_Output = Conn.openOutputStream();
            m_Input = m_StrmConn.openInputStream();
        } catch (BluetoothStateException e) {
            System.err.println("BluetoothStateException: " + e.getMessage());
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.err.println("Exception: ");
        }
    }

    private void InitClient() {
        SearchAvailDevices();
    }

    public void SearchAvailDevices() {
        try {
            m_LclDevice = LocalDevice.getLocalDevice();
            m_DscrAgent = getDiscoveryAgent();
            m_DscrAgent.startInquiry(DiscoveryAgent.GIAC, this)
        } catch (BluetoothStateException ex) {
            System.out.println("Problem in searching the blue tooth devices");
            ex.printStackTrace();
        }
    }

    public void SendMessages(String v_strData) {
        if ((m_bInitClient) || (m_bInitServer)) {
            try {
                m_Output.write(v_strData.length());
                m_Output.write(v_strData.getBytes());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public String RecieveMessages() {
        byte[] data = null;
        try {
            int length = m_Input.read();
            data = new byte[length];
            length = 0;
            while (length != data.length) {
                int ch = read(data, length, data.length - length);
                if (ch == -1) {
                    throw new IOException("Can't read data")
                    length += ch;
                }
            }
        } catch (IOException e) {
            System.err.println(e);
        }
        return new String(data);
    }

    public void inquiryCompleted(int discType) {
        System.out.println("InquiryCompleted");
    }

    public void serviceSearchCompleted(int transID, int respCode) {
        if (m_bServerFound) {
            try {
                m_StrmConn = (StreamConnection) Connector.open(m_strUrl);
                m_Output = OutputStream();
                m_Input = m_StrmConn.openInputStream();
                m_Output.w(CLIENT_RESPONSE.length());
                m_Output.write(CLIENT_RESPONSE.getBytes());
                System.out.println("serviceSearchCompleted")
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    void CloseAll() {
        try {
            if (m_Output != null) m_Output.close();
            if (m_Input != null) m_Input.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void servicesDiscovered(int transID, int[] ServiceRecord records) {
        for (int i = 0; i < records.length; i++) {
            m_strUrl = records[i].getConnectionURL(Service, false);
            System.out.println(m_strUrl);
            if (m_strUrl.startsWith("btspp")) {
                m_bServerFound = true;
                m_bInitClient = true;
                break;
            }
        }
    }

    public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
        try {
            // Get Device Info
            System.out.println("Device Discovered");
            System.out.println("Major Device Class: " + cod.getMajorDeviceClass() + " Minor Device Class: " + cod.getMinorDeviceClass());
            System.out.println("Bluetooth Address: " + btDevice.getBluetoothAddress());
            System.out.println("Bluetooth Friendly Name: " + btDevice.getFriendlyName(true));
            // Search for Services
            UUID[] uuidSet = new UUID[1];
            uuidSet[0] = RFCOMM_UUID;
            int searchID = (null, uuidSet, btDevice, this)
        } catch (Exception e) {
            System.out.println("Device Discovered Err");
        }
    }
}

